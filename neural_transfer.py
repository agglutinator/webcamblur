# code is taken from https://github.com/tensorflow/models/blob/master/research/nst_blogpost/4_Neural_Style_Transfer_with_Eager_Execution.ipynb

import cv2
import os
import time
import numpy as np
import tensorflow as tf

from tensorflow.python.keras.preprocessing import image as kp_image
from tensorflow.python.keras import models 
from tensorflow.python.keras import losses
from tensorflow.python.keras import layers
from tensorflow.python.keras import backend as K

# loss function
def get_content_loss(base_content, target):
    return tf.reduce_mean(tf.square(base_content - target))


def gram_matrix(input_tensor):
    channels = int(input_tensor.shape[-1])
    a = tf.reshape(input_tensor, [-1, channels])
    n = tf.shape(a)[0]
    gram = tf.matmul(a, a, transpose_a=True)
    return gram / tf.cast(n, tf.float32)


def get_style_loss(base_style, gram_target):
    height, width, channels = base_style.get_shape().as_list()
    gram_style = gram_matrix(base_style)
    return tf.reduce_mean(tf.square(gram_style - gram_target))


class NeuralTransfer:
    def __init__(self, gpuid, camera_size, style_image_path=None):
        self.content_layers = ['block5_conv2']
        self.style_layers = [
            'block1_conv1',
            'block2_conv1',
            'block3_conv1',
            'block4_conv1',
            'block5_conv1'
        ]
        self.num_content_layers = len(self.content_layers)
        self.num_style_layers = len(self.style_layers)
        self._gpu_id = gpuid
        self._camera_size = camera_size
        if style_image_path:
            self._style_image = self._load_image(style_image_path, self._camera_size)
            self._style_image = np.expand_dims(self._style_image, axis=0)
        else:
            self._style_image = None
        self._init_tensor_flow(self._gpu_id)

    @staticmethod
    def _init_tensor_flow(gpu_id):
        # tf.enable_eager_execution()
        os.environ["CUDA_VISIBLE_DEVICES"] = f'{gpu_id}'
        tf.device(tf.DeviceSpec(device_type="GPU", device_index=gpu_id))

    @staticmethod
    def _load_image(image_path, camera_size):
        img = cv2.imread(image_path)
        img = cv2.resize(img, camera_size)
        img = tf.keras.applications.vgg19.preprocess_input(img)
        return img

    def _get_model(self):
        vgg = tf.keras.applications.vgg19.VGG19(include_top=False,
                                                weights='imagenet')
        vgg.trainable = False
        style_outputs = [vgg.get_layer(name).output for name in self.style_layers]
        content_outputs = [vgg.get_layer(name).output for name in
                           self.content_layers]
        model_outputs = style_outputs + content_outputs
        return models.Model(vgg.input, model_outputs)

    def get_feature_representations(self, model, frame):
        """Helper function to compute our content and style feature representations.

        This function will simply load and preprocess both the content and style
        images from their path. Then it will feed them through the network to obtain
        the outputs of the intermediate layers.

        Arguments:
          model: The model that we are using.
          frame: The frame to process.

        Returns:
          returns the style features and the content features.
        """
        # batch compute content and style features
        stack_images = np.concatenate([self._style_image, frame], axis=0)
        model_outputs = model(stack_images)

        # Get the style and content feature representations from our model
        style_features = [style_layer[0] for style_layer in
                          model_outputs[:self.num_style_layers]]
        content_features = [content_layer[1] for content_layer in
                            model_outputs[self.num_style_layers:]]
        return style_features, content_features


    def compute_loss(self, model, loss_weights, init_image, gram_style_features,
                     content_features):
        """This function will compute the loss total loss.

        Arguments:
          model: The model that will give us access to the intermediate layers
          loss_weights: The weights of each contribution of each loss function.
            (style weight, content weight, and total variation weight)
          init_image: Our initial base image. This image is what we are updating with
            our optimization process. We apply the gradients wrt the loss we are
            calculating to this image.
          gram_style_features: Precomputed gram matrices corresponding to the
            defined style layers of interest.
          content_features: Precomputed outputs from defined content layers of
            interest.

        Returns:
          returns the total loss, style loss, content loss, and total variational loss
        """
        style_weight, content_weight = loss_weights

        # Feed our init image through our model. This will give us the content and
        # style representations at our desired layers. Since we're using eager
        # our model is callable just like any other function!
        model_outputs = model(init_image)

        style_output_features = model_outputs[:self.num_style_layers]
        content_output_features = model_outputs[self.num_style_layers:]

        style_score = 0
        content_score = 0

        # Accumulate style losses from all layers
        # Here, we equally weight each contribution of each loss layer
        weight_per_style_layer = 1.0 / float(self.num_style_layers)
        for target_style, comb_style in zip(gram_style_features,
                                            style_output_features):
            style_score += weight_per_style_layer * get_style_loss(comb_style[0],
                                                                   target_style)

        # Accumulate content losses from all layers
        weight_per_content_layer = 1.0 / float(self.num_content_layers)
        for target_content, comb_content in zip(content_features,
                                                content_output_features):
            content_score += weight_per_content_layer * get_content_loss(
                comb_content[0], target_content)

        style_score *= style_weight
        content_score *= content_weight

        # Get total loss
        loss = style_score + content_score
        return loss, style_score, content_score

    def compute_grads(self, cfg):
        with tf.GradientTape() as tape:
            all_loss = self.compute_loss(**cfg)
        # Compute gradients wrt input image
        total_loss = all_loss[0]
        return tape.gradient(total_loss, cfg['init_image']), all_loss

    def run_style_transfer(self,
                           frame,
                           num_iterations=1000,
                           content_weight=1e3,
                           style_weight=1e-2):
        display_num = 5
        # We don't need to (or want to) train any layers of our model, so we set their trainability
        # to false.
        model = self._get_model()
        for layer in model.layers:
            layer.trainable = False

        # Get the style and content feature representations (from our specified intermediate layers)
        style_features, content_features = self.get_feature_representations(model, frame)
        gram_style_features = [gram_matrix(style_feature) for style_feature in
                               style_features]

        # Set initial image
        init_image = tf.Variable(frame, dtype=tf.float32)
        # Create our optimizer
        opt = tf.keras.optimizers.Adam(learning_rate=10.0)

        # For displaying intermediate images
        iter_count = 1

        # Store our best result
        best_loss, best_img = float('inf'), None

        # Create a nice config
        loss_weights = (style_weight, content_weight)
        cfg = {
            'model': model,
            'loss_weights': loss_weights,
            'init_image': init_image,
            'gram_style_features': gram_style_features,
            'content_features': content_features
        }

        # For displaying
        num_rows = (num_iterations / display_num) // 5
        start_time = time.time()
        global_start = time.time()

        norm_means = np.array([103.939, 116.779, 123.68])
        min_vals = -norm_means
        max_vals = 255 - norm_means
        for i in range(num_iterations):
            grads, all_loss = self.compute_grads(cfg)
            loss, style_score, content_score = all_loss
            # grads, _ = tf.clip_by_global_norm(grads, 5.0)
            opt.apply_gradients([(grads, init_image)])
            clipped = tf.clip_by_value(init_image, min_vals, max_vals)
            init_image.assign(clipped)
            end_time = time.time()

            if loss < best_loss:
                # Update best loss and best image from total loss.
                best_loss = loss
                best_img = init_image.numpy()

            if i % display_num == 0:
                print('Iteration: {}'.format(i))
                print('Total loss: {:.4e}, '
                      'style loss: {:.4e}, '
                      'content loss: {:.4e}, '
                      'time: {:.4f}s'.format(loss, style_score, content_score,
                                             time.time() - start_time))
                start_time = time.time()
                # Display intermediate images
                if iter_count > num_rows * 5:
                    continue

                iter_count += 1
        print('Total time: {:.4f}s'.format(time.time() - global_start))

        return best_img, best_loss

    def process_frame(self, frame, show_parameters=False):
        exp_frame = np.expand_dims(frame, axis=0)
        frame = tf.keras.applications.vgg19.preprocess_input(exp_frame)
        best_img, best_loss = self.run_style_transfer(exp_frame, num_iterations=30)
        best_img = np.squeeze(best_img, axis=0).astype('uint8')
        return best_img

    def change_contrast(self, frame, contrast):
        if contrast != 0:
            Alpha = float(131 * (contrast + 127)) / (127 * (131 - contrast))
            Gamma = 127 * (1 - Alpha)
            return cv2.addWeighted(frame, Alpha, frame, 0, Gamma)
        else:
            return frame
