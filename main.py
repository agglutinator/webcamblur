#!/usr/bin/env python3
__author__ = "Dmitry Mityaev"
__email__ = "dmitry.v.mityaev@gmail.com"
__version__ = "1.0"
__status__ = "Development"

import argparse
import re

import cv2
import numpy as np

import pyfakewebcam

from neural_transfer import NeuralTransfer
from process_background import ProcessBackground


def main(flags):
    cap = cv2.VideoCapture(flags.cameraindex)
    cap_video = None

    if flags.resolution:
        match = re.search("(\d+)x(\d+)", flags.resolution)
        if match:
            target_width = int(match.group(1))
            target_height = int(match.group(2))
            cap.set(cv2.CAP_PROP_FRAME_WIDTH, target_width)
            cap.set(cv2.CAP_PROP_FRAME_HEIGHT, target_height)
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    print(f"input camera set to {width}x{height} frame size")
    camera_size = (width, height)
    camera = pyfakewebcam.FakeWebcam(flags.cameravirtual, width, height)

    if flags.styletransfer:
        processor = NeuralTransfer(flags.gpuid, camera_size, flags.styletransfer)
    else:
        if flags.video:
            cap_video = cv2.VideoCapture(flags.video)
        processor = ProcessBackground(flags.gpuid, flags.model, camera_size, flags.nframes)
        if flags.background and not flags.video:
            processor.set_background(flags.background)

    _contrast = 0
    _apply_filter = True
    while True:
        _, frame = cap.read()
        if flags.video:
            _, video_frame = cap_video.read()
            if type(video_frame) is np.ndarray:
                processor.set_video_background(video_frame)
            else:
                cap_video.set(cv2.CAP_PROP_POS_FRAMES, 0)
        # cv2.imshow("Webcam Original", frame)
        # normilize picture
        frame = cv2.normalize(frame, frame, 1, 250, cv2.NORM_MINMAX)
        if _apply_filter:
            img = processor.process_frame(frame, flags.performance)
        else:
            img = frame
        img = processor.change_contrast(img, _contrast)

        cv2.imshow("Webcam", img)
        if flags.convert:
            if 'COLOR_RGB2BGR' in flags.convert:
                img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
        if img.shape[2] > 3:
            img = img[:, :, :-1]
        camera.schedule_frame(img)

        # handle keyboard input
        response = cv2.waitKey(1)
        if response == ord('q'):
            break
        if response == ord('f'):
            _apply_filter = not _apply_filter
        if response == ord('.'):
            _contrast -= 1
        if response == ord(','):
            _contrast += 1

    cap.release()
    if cap_video:
        cap_video.release()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-m', '--model',
        type=str,
        required=True,
        help='A path to Tensorflow Lite model.'
    )
    parser.add_argument(
        '-ci', '--cameraindex',
        type=int,
        required=False,
        default=0,
        help='Real/Web camera index.'
    )
    parser.add_argument(
        '-cv', '--cameravirtual',
        type=str,
        required=True,
        help='Virtual camera device to output the application result.'
    )
    parser.add_argument(
        '-gi', '--gpuid',
        type=int,
        required=False,
        default=0,
        help='GPU Id to use.'
    )
    parser.add_argument(
        '-n', '--nframes',
        type=int,
        required=False,
        default=10,
        help='Number of frames between mask generation.'
    )
    parser.add_argument(
        '-p', '--performance',
        required=False,
        action='store_true',
        default=False,
        help='Show performance values.'
    )
    parser.add_argument(
        '-b', '--background',
        type=str,
        required=False,
        help='Background image. Switches off blurring.'
    )
    parser.add_argument(
        '-st', '--styletransfer',
        type=str,
        required=False,
        help='Style image to apply thto e web camera input. Switches off blurring, --background is ignored. Experimental, works extremely slow.'
    )
    parser.add_argument(
        '-v', '--video',
        type=str,
        required=False,
        help='Play this video as background. Switches off blurring and --background.'
    )
    parser.add_argument(
        '-r', '--resolution',
        type=str,
        required=False,
        help='Try to set the webcamera to a certain resolution, WxH format.'
    )
    parser.add_argument(
        '-co', '--convert',
        type=str,
        required=False,
        help='Convert color space. The argument is a string representation of the OpenCV enum ColorConversionCodes: https://docs.opencv.org/master/d8/d01/group__imgproc__color__conversions.html.'
             ' Currently supports COLOR_RGB2BGR only.'
    )

    flags = parser.parse_args()
    main(flags)
