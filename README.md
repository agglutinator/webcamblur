# WebCamBlur #

A project to create a virtual camera having background blurred or replaced by a picture/video.

Inspired by https://github.com/floe/backscrub

Implemented in Python only.


## Dependencies ##
    OpenCV 2 (https://github.com/opencv/opencv):
      pip install opencv-python 

    TensorFlow (https://github.com/tensorflow/tensorflow):
      pip install tensorflow

    v4l2loopback (https://github.com/umlaeute/v4l2loopback):
      sudo apt install -y v4l2loopback-dkms v4l2loopback-utils
    
    pyfakewebcam (https://github.com/jremmons/pyfakewebcam)
      clone https://github.com/jremmons/pyfakewebcam.git
      cd pyfakewebcam
      python setup.py install

## Set up ##
Run 

    sudo modprobe v4l2loopback devices=1

to add a virtual camera, determine the device which will be used to translate the application result to.
For example, your virtual camera is /dev/video2, then the run command line should be:

    python main.py -m models/lite-model_deeplabv3_1_metadata_2.tflite -ci 0 -cv /dev/video2 -p -n 5 -co COLOR_RGB2BGR

Command line parameters:

    usage: main.py [-h] -m MODEL [-ci CAMERAINDEX] -cv CAMERAVIRTUAL [-gi GPUID] [-n NFRAMES] [-p] [-b BACKGROUND]
                   [-st STYLETRANSFER] [-v VIDEO] [-r RESOLUTION] [-co CONVERT]

    optional arguments:
        -h, --help            show this help message and exit
        -m MODEL, --model MODEL
                              A path to Tensorflow Lite model.
        -ci CAMERAINDEX, --cameraindex CAMERAINDEX
                              Real/Web camera index.
        -cv CAMERAVIRTUAL, --cameravirtual CAMERAVIRTUAL
                              Virtual camera device to output the application result.
        -gi GPUID, --gpuid GPUID
                              GPU Id to use.
        -n NFRAMES, --nframes NFRAMES
                              Number of frames between mask generation.
        -p, --performance     Show performance values.
        -b BACKGROUND, --background BACKGROUND
                              Background image. Switches off blurring.
        -st STYLETRANSFER, --styletransfer STYLETRANSFER
                              Style image to apply thto e web camera input. Switches off blurring, --background is ignored. Experimental, works extremely slow.
        -v VIDEO, --video VIDEO
                              Play this video as background. Switches off blurring and --background.
        -r RESOLUTION, --resolution RESOLUTION
                              Try to set the webcamera to a certain resolution, WxH format.
        -co CONVERT, --convert CONVERT
                              Convert color space. The argument is a string representation of the OpenCV enum ColorConversionCodes:
                              https://docs.opencv.org/master/d8/d01/group__imgproc__color__conversions.html. Currently supports COLOR_RGB2BGR only.


## Keyboard input ##

q - quit the application

f - switch filter on/off

,. - change contrast

## TODO ##
1. Build and installation instructions
2. Make the mask applying smoother - done
3. Try BodyPix DL model
4. Add Qt to manage parameters
5. Add picture as background - done
6. Add video as background - done
7. Normilize image before applying model
