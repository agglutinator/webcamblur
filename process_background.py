import cv2
import numpy as np
import os
import sys
import time
import tensorflow as tf
import tensorflow.keras as k

from enum import Enum


KSIZE = (30, 30)

LABELS = ["background", "aeroplane", "bicycle", "bird", "boat", "bottle", "bus", "car", "cat", "chair", "cow",
           "dining table", "dog", "horse", "motorbike", "person", "potted plant", "sheep", "sofa", "train", "tv"]


class RunMode(Enum):
    BLUR = 0  # blur background, default
    BACK = 1  # substitute background by image
    VIDEO = 2  # substitute background by video


class ProcessBackground:
    def __init__(self, gpuid, model_path, camera_size, frame_gap=10):
        self._interpreter, self._input_details, self._output_details = self._init_tensor_flow(gpuid, model_path)
        self.run_mode = RunMode.BLUR
        self._background_image = None
        self._camera_size = camera_size
        self._frame_count = 0
        self._frame_gap = frame_gap
        self._last_time = time.time()
        self._current_mask = None

    def _init_tensor_flow(self, gpu_id, model_path):
        os.environ["CUDA_VISIBLE_DEVICES"] = f'{gpu_id}'
        tf.device(tf.DeviceSpec(device_type="GPU", device_index=gpu_id))
        interpreter = tf.lite.Interpreter(model_path=model_path)
        interpreter.allocate_tensors()
        input_details = interpreter.get_input_details()
        output_details = interpreter.get_output_details()
        return interpreter, input_details, output_details

    def _get_mask(self,
                  interpreter,
                  input_details,
                  output_details,
                  img,
                  mask_size):
        input_mean = 127.5
        input_std = 127.5

        height = input_details[0]['shape'][1]
        width = input_details[0]['shape'][2]
        img_small = cv2.resize(img, (width, height))
        input_data = np.expand_dims(img_small, axis=0)
        input_data = (np.float32(input_data) - input_mean) / input_std
        interpreter.set_tensor(input_details[0]['index'], input_data)
        interpreter.invoke()
        output_data = interpreter.get_tensor(output_details[0]['index'])
        results = np.squeeze(output_data)
        out = k.backend.argmax(results, 2)
        pers_idx = LABELS.index("person")
        mask = np.where(out == pers_idx, 255, 0).astype(np.uint8)
        mask = cv2.resize(mask, mask_size)
        mask = cv2.blur(mask, KSIZE)
        return mask

    def _apply_mask(self, img, mask, background):
        normalized_background = background.astype(float) / 255
        tmp_mask = mask.astype(float) / 255
        shape = tmp_mask.shape
        normalized_mask = np.empty((*shape, 3))
        for plane in range(3):
            normalized_mask[:, :, plane] = tmp_mask
        normalized_img = img.astype(float) / 255
        person = cv2.multiply(normalized_mask, normalized_img)
        background_out = cv2.multiply(1 - normalized_mask, normalized_background)
        img_out = (cv2.add(person, background_out) * 255).astype(np.uint8)
        return img_out

    def set_background(self, background_path):
        background_image = cv2.imread(background_path)
        self._background_image = cv2.resize(background_image, self._camera_size)
        self.run_mode = RunMode.BACK

    def set_video_background(self, video_frame):
        self._background_image = cv2.resize(video_frame, self._camera_size)
        self.run_mode = RunMode.VIDEO

    def clear_background(self):
        self._background_image = None
        self.run_mode = RunMode.BLUR

    def process_frame(self, frame, show_parameters=False):
        if (self._frame_count == 0) or (self._frame_count % self._frame_gap == 0):
            self._current_mask = self._get_mask(
                self._interpreter,
                self._input_details,
                self._output_details,
                frame,
                self._camera_size
            )
            if self._frame_count == sys.maxsize:
                self._frame_count = 0
        self._frame_count += 1
        if (self.run_mode == RunMode.BACK or self.run_mode == RunMode.VIDEO) and type(self._background_image) == np.ndarray:
            out_frame = self._apply_mask(frame, self._current_mask, self._background_image)
        elif self.run_mode == RunMode.BLUR:
            # blurred_frame = np.squeeze(tfa.image.gaussian_filter2d(img, filter_shape=11, sigma=15.0))
            blurred_frame = cv2.blur(frame, KSIZE)
            blurred_frame = cv2.medianBlur(blurred_frame, 37)
            out_frame = self._apply_mask(frame, self._current_mask, blurred_frame)
        else:
            # if mode is not determined, just bypass
            out_frame = frame
        if show_parameters:
            current_time = time.time()
            # print(f"current: {current_time}, last : {self._last_time}")
            text = f"FPS: {int(1 / (current_time - self._last_time))}"
            cv2.putText(out_frame, text, (10, 20), cv2.FONT_HERSHEY_PLAIN, 2,
                        (0, 255, 0), 2)
            self._last_time = current_time
        return out_frame

    def change_contrast(self, frame, contrast):
        if contrast != 0:
            Alpha = float(131 * (contrast + 127)) / (127 * (131 - contrast))
            Gamma = 127 * (1 - Alpha)
            return cv2.addWeighted(frame, Alpha, frame, 0, Gamma)
        else:
            return frame
